#!/bin/bash
#
# Descr: Count the number of the word "de" in FILE
#
# Usage: ./wordfreq.sh FILE

TEXT=$1
# split by space and count the number of occurences of 'de'
cat $TEXT | tr ' ' '\n' | grep -i -o -c '\<de\>' 
