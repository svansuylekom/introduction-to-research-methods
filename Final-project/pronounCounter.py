import nltk
import os
from xml.etree import ElementTree

def readNewspapers():
    ''' Read 10.000 tokens from newspapers' text from TwNC and return them in a list '''
    dirs, files, data = [], [], []
    path = "Data/TwnC/"
    for directory in os.listdir(path):
        dirs.append(directory)
    print("### Extracting newspaper data ...")
    for dir in dirs:
        directory = path + dir + '/' 
        filenames = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]
        for filename in filenames:
            with open(directory + filename, 'r') as fil:
                if len(data) < 10000:
                    print(" - {} read".format(filename))
                    xml_tree = ElementTree.parse(fil)
                    # Read text from only the content elements in newspapers
                    for node in xml_tree.findall("./artikel/body/le/p"):
                        try:
                            tokens = nltk.word_tokenize(node.text.rstrip())
                            data += tokens
                        except AttributeError:
                            pass
                else:
                    return data
    return data

def readTweets(filename="Data/Twitter/00"):
    ''' Read all the tweets from filename and extract 10.000 tokens '''
    data = []
    with open(filename,"r") as f:
        print("### Extracting Twitter data ...")
        for line in f:
            if len(data) < 100000:
                data += nltk.word_tokenize(line.rstrip())
        print(" - {} read".format(filename))
    return data                

def countPronouns(data):
    ''' Count the number of Dutch personal pronouns that occur in data
        Return the total number of PPs and a dict with counts for all PPs'''
    c = 0 
    nr_pronouns = 0
    pronoun_counts = {}
    pronouns = ["ik","je","jij","jou","u","hij","zij","ze",
                "wij","we","jullie","mij","me","hun","haar",
                "ons","hen"]
    for pronoun in pronouns:
        pronoun_counts[pronoun] = 0    
    for word in data:
        if c >= 10000:
            break #ensure than you use exactly 10.000 tokens
        word = word.lower()
        if word in pronouns:
            nr_pronouns += 1
            pronoun_counts[word] += 1
        c += 1 
    return nr_pronouns, pronoun_counts

def main():
    # Count and print the number of personal pronouns for Twitter
    twitterdata = readTweets()
    nrTwitterPronouns, twitterPronouns = countPronouns(twitterdata)
    print("\n### Finished analyzing Twitter data:\n - total nr. of pronouns in twitter: {}"
        .format(nrTwitterPronouns))
    print(" - Distribution of personal pronouns:")
    for k, v in twitterPronouns.items():
        print("\t- {:10s}{}".format(k, v))
    # Count and print the number of personal pronouns for newspapers
    newspaperdata = readNewspapers()
    nrNewspaperPronouns, newspaperPronouns = countPronouns(newspaperdata)
    print("\n### Finished analyzing newspaper data:\n - total nr. of pronouns: {}"
        .format(nrNewspaperPronouns))
    print(" - Distribution of personal pronouns:")
    for k, v in newspaperPronouns.items():
        print("\t- {:10s}{}".format(k, v))    

if __name__ == "__main__":
    main()